package com.filos.dtos;

import lombok.Data;

@Data
public class CastingDto{
    private String language;
    private String director;
    private String writer;
    private String productionCompany;
}
