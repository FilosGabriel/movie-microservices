package com.filos.controller;

import com.google.common.util.concurrent.AbstractService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AbstractBasicController{
    private final AbstractService service;
}
