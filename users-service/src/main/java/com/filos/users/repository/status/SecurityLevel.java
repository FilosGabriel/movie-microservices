package com.filos.users.repository.status;

public enum SecurityLevel {
    FIRST_LEVEL,
    TWO_FACTOR_AUTHENTICATION
}
