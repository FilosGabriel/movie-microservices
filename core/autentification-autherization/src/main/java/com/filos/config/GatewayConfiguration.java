package com.filos.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.filos.api")
public class GatewayConfiguration{

}
